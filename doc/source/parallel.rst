===============
Parallel Module
===============

.. rubric:: Overview

.. autosummary::
    :nosignatures:

    freud.parallel.NumThreads
    freud.parallel.setNumThreads

.. rubric:: Details

.. automodule:: freud.parallel
    :synopsis: Manage TBB thread usage.
    :members:

    .. automethod:: freud.parallel.setNumThreads
